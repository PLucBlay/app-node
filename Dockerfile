FROM node:alpine

# create app directory
RUN mkdir /app
WORKDIR /app

# copy files and install app
COPY package.json /app
RUN npm install
COPY src /app/src
COPY config.json /app

# exec app on container start-up
CMD [ "node", "src/app.js" ]

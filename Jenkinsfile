pipeline {

  agent { node { label 'node-prod' } }

  environment { registry = 'registry.gitlab.com/ajcpiroma/app-node/node-'
                registryCredential = 'cred4gitlab' }

  stages{

    stage('CODE') {
        steps { git branch: 'master', credentialsId: '7031c512-7d39-4d85-818a-926cdc04d1ca', url: 'git@gitlab.com:ajcpiroma/app-node.git' }
    }
    
    stage('BUILD') {
        steps { sh 'npm install' }
    }
    
    stage('TEST') {
        steps { sh 'npm test tests/tests.js' }
        post {
          success { slackSend color: 'good', message: "$JOB_NAME (build $BUILD_NUMBER): Tests successful. Good job!" }
          failure { slackSend color: 'danger', message: "$JOB_NAME (build $BUILD_NUMBER): Tests unsuccessful. Bad job!" } }
    }
    
    stage('RELEASE') {
        steps { script { 
                dockerImage1 = docker.build registry + "app:prod-$BUILD_NUMBER"
                dockerImage2 = docker.build( registry + "db:prod-$BUILD_NUMBER", "./database" )
                docker.withRegistry( 'https://registry.gitlab.com', registryCredential ) { dockerImage1.push() }
                docker.withRegistry( 'https://registry.gitlab.com', registryCredential ) { dockerImage2.push() } } }
    }
   
    stage('DEPLOY') {
        steps { 
                sh 'docker stop nd-db ; docker rm nd-db'
                sh 'docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d --force-recreate' }
    }
  }

  post {
    always { slackSend color: '#006699', message: "$JOB_NAME (build $BUILD_NUMBER): CI/CD done." }
    failure { slackSend color: 'danger', message: "$JOB_NAME (build $BUILD_NUMBER): Deployment failed." }
    success { slackSend color: 'good', message: "$JOB_NAME (build $BUILD_NUMBER): Deployment successful." }
  }
}

